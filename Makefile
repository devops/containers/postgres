SHELL = /bin/bash

pg_version ?= 12
build_tag ?= postgres-$(pg_version)
base_image = postgres:$(pg_version)

# Duplicates list from debian-buster
packages = curl gosu less locales nano ncat vim wait-for-it

.PHONY : build
build:
	DOCKER_BUILDKIT=1 docker build --pull -t $(build_tag) \
		--build-arg base_image=$(base_image) \
		--build-arg packages="$(packages)" \
		--shm-size=256MB \
		./src

.PHONY : clean
clean:
	echo 'no-op'

.PHONY : test
test:
	build_tag=$(build_tag) ./test.sh
