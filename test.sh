#!/bin/bash

container="postgres-${CI_COMMIT_SHORT_SHA:-local}-test"
volume="${container}-volume"

echo "Creating test volume"
docker volume create $volume

echo "Starting test image ..."
docker run --rm --name $container -d \
       -e POSTGRES_PASSWORD=testing \
       -v $volume:/var/lib/postgresql/data \
       $build_tag

code=1
interval=5
SECONDS=0
while [[ $SECONDS -lt 60 ]]; do
    echo -n "Checking health status ... "
    status=$(docker inspect -f '{{.State.Health.Status}}' $container)
    echo $status
    case $status in
        healthy)
	    code=0
            break
            ;;
        unhealthy)
            break
            ;;
        starting)
            sleep $interval
            ;;
        *)
            echo "Unexpected status."
            break
            ;;
    esac
done

echo "Cleaning up ..."
docker stop $container
docker volume rm $volume

if [[ $code -eq 0 ]]; then
    echo "SUCCESS"
else
    echo "FAILURE"
fi

exit $code
